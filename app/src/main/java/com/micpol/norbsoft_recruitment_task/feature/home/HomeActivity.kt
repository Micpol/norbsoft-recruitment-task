package com.micpol.norbsoft_recruitment_task.feature.home

import android.os.Bundle
import com.micpol.norbsoft_recruitment_task.R
import com.micpol.norbsoft_recruitment_task.architecture.BaseActivity

class HomeActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

}
