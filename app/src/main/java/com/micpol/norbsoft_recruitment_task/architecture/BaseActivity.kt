package com.micpol.norbsoft_recruitment_task.architecture

import dagger.android.support.DaggerAppCompatActivity

abstract class BaseActivity : DaggerAppCompatActivity()
