package com.micpol.norbsoft_recruitment_task.architecture

import dagger.android.support.DaggerFragment

abstract class BaseFragment : DaggerFragment()
